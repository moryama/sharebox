import { Item } from '@prisma/client';

export type PublicItem = Pick<
  Item,
  'id' | 'name' | 'description' | 'photo' | 'isFree' | 'price' | 'status'
>;
