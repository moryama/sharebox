import React from 'react';
import Header from '@/components/Header';
import ItemList from '@/components/ItemList';
import './globals.css';

const headerText =
  'Some general information about the giveaway, for example reasons for giveaway, a bit of context on the items, and other stuff. It can be a few lines long, but not too long.';

const Home = () => {
  return (
    <div className="flex flex-col gap-20">
      <Header text={headerText} />
      <ItemList />
    </div>
  );
};

export default Home;
