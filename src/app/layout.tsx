import './globals.css';
import type { Metadata } from 'next';
import { Inter } from 'next/font/google';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'Sharebox',
  description: 'Page to share giveaway items',
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body
        className={`${inter.className} p-10 md:p-15 lg:p-16 min-h-screen  bg-slate-100`}
      >
        {children}
      </body>
    </html>
  );
}
