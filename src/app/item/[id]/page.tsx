import ContactForm from '@/components/ContactForm';
import data from '@/data.json';
import Image from 'next/image';
import Link from 'next/link';

export async function generateStaticParams() {
  return data.map(data => ({
    slug: data.id,
  }));
}

const getItemById = (id: number) => {
  return data.find(item => item.id === id);
};

export default function Page({ params }: { params: { id: string } }) {
  const item = getItemById(Number(params.id));

  if (!item) {
    return <p>Item not found.</p>;
  }

  return (
    <div className="max-w-3xl mx-auto p-6 space-y-6">
      <Link
        href="/"
        className="text-sm font-medium text-gray-500 hover:text-gray-700"
      >
        ← Back
      </Link>
      <Image
        src="/placeholder.svg"
        alt={item.name}
        width="600"
        height="400"
        layout="responsive"
        className="rounded-lg"
      />
      <h1 className="text-3xl font-semibold">{item.name}</h1>
      <p className="text-lg text-gray-700">{item.description}</p>
      <p className="text-xl font-medium mt-4">
        Price: {item.isFree ? `Free` : `$${item.price}`}
      </p>
      <h2 className="text-2xl font-medium mt-6">Pick up info</h2>
      <p className="text-lg text-gray-600">
        Some general information about the giveaway, for example reasons
      </p>
      <ContactForm />
    </div>
  );
}
