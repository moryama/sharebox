import data from '@/data.json';
import ItemCard from '@/components/ItemCard/ItemCard';
import '../app/globals.css';
import { PublicItem } from '@/types';

const ItemList = () => {
  return (
    <div className="grid gap-12 md:grid-cols-1 lg:grid-cols-2 xl:grid-cols-3">
      {data.map(item => {
        return <ItemCard item={item as PublicItem} key={item.id} />;
      })}
    </div>
  );
};

export default ItemList;
