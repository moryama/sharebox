import '../app/globals.css';

type HeaderProps = {
  text: string;
};

const Header = ({ text }: HeaderProps) => {
  return <p className="text-lg md:text-2xl md:w-1/2">{text}</p>;
};

export default Header;
