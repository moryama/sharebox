import { ItemStatus } from '@prisma/client';
import Link from 'next/link';

type FooterProps = {
  id: number;
  isFree: boolean;
  price?: number;
  status: ItemStatus;
};

const Footer = ({ id, isFree, price, status }: FooterProps) => {
  return (
    <div className="flex justify-between items-center mt-2">
      <span className="font-medium text-gray-700">
        {isFree ? 'Free' : `$${price}`}
      </span>
      {status === 'AVAILABLE' ? (
        <Link href={`/item/${id}`}>
          <button className="px-4 py-2 bg-black text-white rounded-md hover:bg-gray-800">
            Reserve
          </button>
        </Link>
      ) : (
        <span className="text-red-600 font-medium">Reserved</span>
      )}
    </div>
  );
};

export default Footer;
