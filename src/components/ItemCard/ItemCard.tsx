import React from 'react';
import Image from 'next/image';
import '../../app/globals.css';
import { PublicItem } from '@/types';
import Footer from './Footer';
import Link from 'next/link';

type ItemCardProps = {
  item: PublicItem;
};

const ItemCard = ({ item }: ItemCardProps) => {
  return (
    <div className="flex flex-col justify-between h-full w-full p-4 border rounded-md bg-white space-y-2">
      <div>
        <Image
          src="/placeholder.svg"
          alt={item.name}
          width="400"
          height="300"
          layout="responsive"
          className="rounded-md"
        />
        <h2 className="text-2xl font-semibold mt-2">{item.name}</h2>
        <p className="text-base mt-2">{item.description}</p>
        <Link
          href={`/item/${item.id}`}
          className="text-black font-medium hover:text-gray-600 mt-2 block"
        >
          Read more
        </Link>
      </div>
      <Footer
        id={item.id}
        isFree={item.isFree}
        status={item.status}
        price={item.price}
      />
    </div>
  );
};

export default ItemCard;
