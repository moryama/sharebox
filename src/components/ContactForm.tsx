'use client';
import React, { useState } from 'react';

const ContactForm = () => {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    message: '',
  });

  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    // Do something with the form data
    console.log(formData);
  };

  return (
    <form
      onSubmit={handleSubmit}
      className="space-y-4 p-4 border rounded-md bg-gray-200"
    >
      <div className="flex flex-col">
        <label htmlFor="name" className="text-lg">
          Name <span className="text-red-500">*</span>
        </label>
        <input
          type="text"
          id="name"
          name="name"
          value={formData.name}
          onChange={handleInputChange}
          className="border rounded p-2"
          required
        />
      </div>

      <div className="flex flex-col">
        <label htmlFor="email" className="text-lg">
          Email <span className="text-red-500">*</span>
        </label>
        <input
          type="email"
          id="email"
          name="email"
          value={formData.email}
          onChange={handleInputChange}
          className="border rounded p-2"
          required
        />
      </div>

      <div className="flex flex-col">
        <label htmlFor="message" className="text-lg">
          Message
        </label>
        <textarea
          id="message"
          name="message"
          placeholder="Hello I'm interested in this item. I could come and pick it up..."
          value={formData.message}
          onChange={handleInputChange}
          className="border rounded p-2 h-24"
        ></textarea>
      </div>

      <div className="text-right">
        <button
          type="submit"
          className="px-4 py-2 bg-blue-500 text-white rounded-md hover:bg-blue-600"
        >
          Send
        </button>
      </div>
    </form>
  );
};

export default ContactForm;
